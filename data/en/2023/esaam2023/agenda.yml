complete: false
types:
    - name: Keynote 
      id: keynote 
      color: "#00FF00"
    - name: Session
      id: session
      color: "#668AA9"
    - name: Sponsor
      id: sponsor
      color: "#DD2867"
    - name: "EclipseCon"
      id: eclipsecon
      color: "#F48E1E"
    - name: ""
      id: organization
      color: "#FFFFFF"
      
sets:    
  - name: Morning
    items:
    - name: 'Introduction and Welcome'
      presenter: <a href="speakers/#alexander_chatzigeorgiou">A. Chatzigeorgiou</a>, <br/><a href="speakers/#philippe_krief">Ph. Krief</a>
      type: organization
      time: 09:00 - 09:10
             
    - name: 'Spurring edge innovation through Open Platforms’'
      presenter:  <a href="speakers/#rolf_riemenschneider">Rolf Riemenschneider</a>
      type: keynote
      time: 09:10 - 09:25
#      abstract: |
#             <p></p>
# Session #1
    - name: 'NEMO: Building the Next Generation Meta Operating System'
      presenter: <a href="speakers/#ioannis_chochliouros">Ioannis Chochliouros</a>
      type: session
      time: 09:25 - 09:45
      abstract: |
             <p>Artificial Intelligence of Things (AIoT) is one of the next big concepts to support societal changes and economic growth, 
             being one of the fastest growing ICT segments. A specific challenge is to leverage existing technology strengths to develop 
             solutions that sustain the European industry and values. ΝΕΜΟ (Next Generation Meta-Operating system) establishes itself as 
             the gamechanger of the AIoT-edge-cloud continuum by introducing an open source, modular and cybersecure meta-operating system, 
             leveraging on existing technologies and introducing novel concepts, methods, tools, testing and engagement campaigns.</p>
             <p>NEMO will bring intelligence closer to the data and make AI-as-a-Service an integral part of network self-organisation and 
             micro-services execution orchestration. Its widespread penetration and massive acceptance will be achieved via new technology, 
             pre-commercial exploitation components and liaison with open-source communities. By defining a modular and adaptable mOS (meta-OS) 
             architecture together with building blocks and plugins the project will address current and future technological and business needs.</p>
      
    - name: 'Scalable and Portable Federated Learning Simulation Engine'
      presenter: <a href="speakers/#borja_arroyo_galende">Borja Arroyo Galende</a>
      type: session
      time: 09:45 - 10:05
      abstract: |
             <p>Federated learning (FL) is one of the most promising approaches to
             ensure privacy in the application of data-driven techniques to sensitive
             information. However, the implementation of such approaches
             in a production environment is still an important challenge. In
             this paper, we present a scalable, portable, hardware-independent,
             model-agnostic FL Simulation Engine (FLSE) with the aim of easing
             the job of researchers who want to train FL models to be deployed
             in production environments.</p>
             <p> The FLSE offers a tool that can be
             used both standalone or embedded within a larger architecture, it
             can be deployed seamlessly and allows concurrent, scalable, and
             highly available V&V assessment support for FL models. The tool
             allows researchers to understand the behaviour, in terms of metric
             performance, of their proposed models in production scenarios,
             allowing a boost in trustworthiness towards ethical AI.</p>
      
    - name: 'Cognitive Architecture for Process industries'
      presenter: <a href="speakers/#peter_bednar">Peter Bednar</a>
      type: session
      time: 10:05 - 10:25
      abstract: |
             <p>This paper introduces a Cross-Sectorial Big Data Processing
             platform which provides tools for the semantic modelling of the
             data analytical processes and for the automatic generation of data
             analysis scripts for solving the described problems.</p>
             <p>The main contribution of this paper is the cognitive component for the
             automatic extraction of the task definition from the narrative
             description of the problem based on the Large Language Models
             (LLMs). We have evaluated the proposed method on five
             problems from the different domains and found that the automatic
             extraction of the task definition can have promising results that
             can be applied to full-automatic data analytics.</p>
      
    - name: 'Break'
      presenter: 
      type: organization
      time: 10:25 - 10:45
      
# Session #2
    - name: 'On the Containerization and Orchestration of RISC-V architectures for Edge-Cloud computing'
      presenter: <a href="speakers/#francesco_lumpp">Francesco Lumpp</a>
      type: session
      time: 10:45 - 11:05
      abstract: |
             <p>Containerization technologies, orchestration systems and
             open hardware architectures, such as RISC-V, are crucial
             as the foundation of open digital infrastructures for the
             computing continuum - the seamless distribution of data
             and computation across platforms with heterogeneous capabilities.
             However, it is unknown how containerization
             technologies and orchestration systems, such as Kubernetes,
             would impact performance in new architectures based on
             RISC-V. This work aims to address this question and introduces
             KubeEdge-V, an orchestration platform for RISC-V
             systems.</p><p> We define the minimum components required to
             support the basic features of the containerization and orchestration
             platforms: network plug-ins, container runtimes and
             computational/networking requirements, and we evaluated
             KubeEdge-V performance on a prototype of a distributed
             computing system based on SiFive processors called Monte
             Cimone. Finally, the paper compares the performance of
             KubeEdge-V on SiFive processors with an equivalent system
             based on ARM architecture featuring the same power envelope.</p>
      
    - name: 'FlexConnect: Mobile Computational Offloading'
      presenter: <a href="speakers/#davide_taibi">Davide Taibi</a>
      type: session
      time: 11:05 - 11:25    
      abstract: |
             <p>Edge computing is increasing its popularity also in non-computing
             specific environments. In particular, Makerspaces are facing the
             issue of providing cognitive-edge to cloud computational power to
             their users, for running their 3D modeling and optimizations. However,
             while the edge-to-cloud offloading might be a traditional issue
             in edge computing systems, some countries require Makerspaces to
             provide their service only during operational hours, and therefore
             to switch off their machines, including their servers, during the
             night. This calls for the need for flexible computational offloading,
             enabling to checkpoint of current tasks and restoring them during
             opening hours.</p>
             <p>This work examines the feasibility of designing and
             implementing such a system, to facilitate computational offloading
             with checkpointing to increase the availability of computational
             power. We propose and prototype the implementation for a system
             that enables a provider to provide computational power through
             their edge with a checkpoint and restore feature to prevent loss of
             progress while the system works on a computation. </p>
             <p>A client-side application was designed and implemented to enable users to access
             the computational offloading service from a provider at a given location
             directly from their smartphone. Results show that it is possible
             to implement a continuous edge-to-cloud computational platform
             to enable full checkpoint and restore of computational tasks while
             providing a good quality of service and using a controlled amount
             of energy</p>
      
    - name: 'Unikernels Motivations, Benefits and Issues: A Multivocal Literature Review'
      presenter: <a href="speakers/#davide_taibi">Davide Taibi</a>
      type: session
      time: 11:25 - 11:45
      abstract: |
             <p>While working in the cloud, the trend is to secure all the resources
             in order for the applications and systems to operate as efficiently
             as possible. Huge amounts of resources are wasted on unnecessary
             utilities and resource-consuming processes, which can rather have
             a negative effect. Unikernels are the last trend in this direction.
             We aim to shed light on the motivations, benefits, and issues in
             unikernels. Our goal is to systematically analyze what made this
             technology so attractive, identifying the benefits they have attained
             and the issues encountered in embracing this new technology. We
             surveyed academic and grey literature by means of the Multivocal
             Literature Review process, analyzing 590 sources, of which 62 reported
             motivations, benefits, and issues. The main motivations for
             adopting unikernels are performance and security, which are also
             reflected in the benefits reported. In terms of issues, the maturity
             of the existing frameworks as well as the tool support, are the main
             challenges that need to be addressed. Given the great potential that
             unikernels can bring in terms of performance and security, further
             research is needed to investigate the pros and cons, how to use
             them, and in which contexts they are beneficial.</p>
      
    - name: 'Lunch with EclipseCon'
      presenter: Bürgersaal 1
      type: eclipsecon
      time: 12:00 - 13:00
             
  - name: Afternoon
    items:
    - name: 'AI and Edge Computing: Driving Competitive Advantage for Businesses'
      presenter: <a href="speakers/#marco_gonzalez_hierro">Marco Conzalez Hierro</a>
      type: keynote
      time: 13:15 - 13:45
      abstract: |
             <p>In this era of rapid technological evolution, businesses are constantly seeking innovative ways to gain 
             a competitive edge and stay ahead of the curve. Edge Computing and Artificial Intelligence have emerged as 
             two pivotal technologies that can revolutionize how industries operate in an increasingly challenging global market. 
             The combination of these two new paradigms is expected to help organizations unlock new opportunities, enhance 
             operational efficiency, and ultimately drive business success.</p>
             <p>Based on IKERLAN’s first-hand experience as members of the Mondragon Corporation—the largest industrial 
             group in the Basque Country (Spain)—this Keynote will focus on examining the practical implementation of 
             edge-cloud solutions in EU strategic sectors such as Industry, Manufacturing, Transport, and Energy. This talk 
             will include real-world examples that demonstrate how open source solutions can contribute significantly to fostering 
             innovation within Europe and to maintaining the competitiveness of European businesses.</p>
             <p>This presentation will also describe a concrete example of successful R&D collaboration revolving around open source
             edge cloud technologies: the COGNIT Project. Funded by the Horizon Europe program, this project will establish a novel 
             distributed Function-as-a-Service model for managing edge applications. This new AI-enabled platform is poised to 
             revolutionize the processing of data by allowing edge and IoT devices to easily offload heavy processing tasks to 
             the emerging multi-provider Cognitive Cloud Continuum.</p>
      
# Session #3
      
    - name: 'Modular Monoliths the way to Standardization'
      presenter: <a href="speakers/#michael_tsechelidis">Michael Tsechelidis</a>
      type: session
      time: 13:45 - 14:05
      abstract: |
             <p>In resent years monolith architecture gains once again a lot
             of popularity, in order to reduce costs and time compared
             to more complicated architectures. Taking into account the
             advantages of micro-service, and trying to embed some of
             them to monolith architectures, we come to the creation of
             modular monoliths. This type of design can be consider quite
             new, and so there isn’t yet a specific architecture design that
             someone could follow if they wish to use it.</p><p>In this paper
             we present an architectural design and an implementation
             strategy for modular monoliths. To evaluate the usefulness
             of this architecture, we have conducted a study, validating
             the design and its implementation. In this study 12 architects
             from different companies took part, expressing some
             concerns regarding the feasibility in bigger project but also
             giving an overall positive feedback for the design.</p>
      
    - name: 'Tool Support for Architectural Pattern Selection and Application in Cloud-centric Service-oriented IDEs'
      presenter: <a href="speakers/#feryal_fulya_horozal">Feryal Fulya Horozal</a>
      type: session
      time: 14:05 - 14:25
      abstract: |
             <p>Architectural patterns are high level design guidelines and principles for software systems. They play a crucial role 
             laying the foundations to the organization and structure of software systems and have high impact on their quality and success 
             both in terms of business and engineering aspects. Deciding for a specific software architecture requires careful analysis of 
             several factors regarding the software system including system characteristics, constraints and required quality attributes, 
             and is often not trivial.</p><p>This paper presents a framework for architectural pattern selection and application that supports 
             the decision-making process of choosing an appropriate architectural pattern, and the organization of the software structure 
             based on the chosen pattern in an automated fashion when integrated in IDEs. In particular, the paper presents how this framework 
             is implemented and integrated within an innovative open source cloud-native integrated development environment.</p>
      
    - name: 'PIACERE Integrated Development Environment'
      presenter: <a href="speakers/#gorka_benguria_elguezabal">Gorka Benguria Elguezabal</a>
      type: session
      time: 14:25 - 14:45
      abstract: |
             <p>This article presents a model-driven engineering (MDE) integrated
             development environment (IDE) to assist the DevSecOps
             (Development Security and Operations) process. This tool has been
             developed within the PIACERE H2020 project, which proposes a
             framework composed of a set of tools developed to support all
             phases of the DevSecOps life cycle including modeling,
             test/validation, build/generate, deployment, operate and modeling.</p>
             <p>PIACERE IDE is an Eclipse based tool, that acts as the front-end
             for this framework, and plays a key role in integrating other
             PIACERE tools. The IDE allows developers to access the different
             tools in a simple and unified way.</p>
      
    - name: 'Break'
      presenter: 
      type: organization
      time: 14:45 - 15:05
      
# Session #4
      
    - name: 'LinkEdge: Open-sourced MLOps Integration with IoT Edge'
      presenter: <a href="speakers/#savidu_dias">Savidu Dias</a>
      type: session
      time: 15:05 - 15:25
      abstract: |
             <p>MLOps, or Machine Learning Operations, play a significant role
             in streamlining production deployment, monitoring, and management
             of machine learning models. Integrating MLOps with edge
             devices poses unique challenges that require customised deployment
             strategies and efficient model optimisation techniques. This
             paper introduces LinkEdge, a set of tools that enable the integration
             of MLOps practices with edge devices. LinkEdge consists of two
             sets of tools: one for setting up infrastructure within edge devices
             to be able to receive, monitor, and run inference on ML models and
             another for MLOps pipelines to package models to be compatible
             with the inference and monitoring components of the respective
             edge devices. </p>
             <p>The LinkEdge platform is evaluated by obtaining a
             public dataset for predicting the breakdown of Air Pressure Systems
             in trucks. Additionally, the platform is compared against a set of
             commercial and open-source tools and services that serve similar
             purposes. The overall performance of LinkEdge matches that of
             already existing tools and services while allowing end users setting
             up Edge-MLOps infrastructure the complete freedom to set up their
             system without entirely relying on third-party licensed software.</p>
      
    - name: 'Enabling Compute and Data Sovereignty with Infrastructure-Level Data Spaces'
      presenter: <a href="speakers/#jacopo_marino">Jacopo Marino</a>
      type: session
      time: 15:25 - 15:45
      abstract: |
             <p>Data is a critical asset in today’s world, and its value cannot be
             overstated. However, ensuring that data is accessible only to authorized
             parties and protecting it against theft present significant
             challenges. A potential solution to these issues is creating data
             spaces that interconnect clusters managed by different actors. The
             latter can securely exchange data under specific constraints and
             terminate connections when needed. </p>
             <p>This paper aims to show how
             to create infrastructure-level data spaces to facilitate secure data
             exchange and prevent data theft. Furthermore, we investigate how
             data sovereignty can be maintained through cluster data exchange,
             which is crucial in an era where data is increasingly regulated
             and controlled. Additionally, we explore how offloading applications
             from the data consumer into the data producer cluster can
             match data gravity patterns, improving overall system efficiency. Finally,
             this paper presents the potential integration of the proposed
             solution within the framework of IDSA and Gaia-X, serving as
             promising option for implementing their proposed functionalities.</p>
      
    - name: 'Feature Estimation for Punching Tool Wear at the Edge'
      presenter: <a href="speakers/#olli_saarela">Olli Saarela</a>
      type: session
      time: 15:45 - 16:05
      abstract: |
             <p>As a fast and inexpensive machining method applicable for creating a wide range of shapes and 
             producing large batches, sheet metal punching is widely used e.g., in automotive, aerospace, electronics, 
             and construction industries. A significant downside of sheet metal punching is the punching tool wear 
             in use. A worn punch tool may impact the quality of the end product by causing imperfections and reduce 
             the efficiency of the manufacturing process through increased scrap and by slowing down the production.</p> 
             <p>Effective monitoring of punching tool wear is therefore essential for an efficient and cost-effective 
             production of high-quality parts. The monitoring can be based on acceleration measurement which produces 
             large amounts of raw data, making edge processing ideal as only the indication of the tool condition needs 
             to be sent forward for decision support. Classification models for tool wear identification were built and 
             compared in this study. The models are based on measured acceleration data. Two different open-source methods 
             for time series feature extraction, namely TSFEL and MiniRocket, were tested and the classification results 
             based on them compared.</p>
             <p>All methods used for building the models are computationally light and therefore 
             applicable for real-time data processing at the edge. According to the results the MiniRocket algorithm is 
             suitable for the task and superior compared to the TSFEL method. The classification accuracies based on the 
             MiniRocket features are at best over 96.5 % and at worst around 84 %, whereas the corresponding accuracies 
             are between 35 and 56 % for TSFEL feature based models. The use of the MiniRocket algorithm in building a model 
             for punch tool monitoring shows very promising results. However, the dataset used was very limited. 
             Therefore, further investigation is required based on an ampler dataset.</p>
      
    - name: 'The European Computing Continuum: Initiatives, Community, and Directions'
      presenter: <a href="speakers/#giovanni_rimassa">Giovanni Rimassa</a>
      type: sponsor
      time: 16:05 - 16:25
      abstract: |
             <p>The convergence megatrend of Cloud Computing and IoT towards a computing continuum has been clearly recognised
             at European level and is having significant innovation, economic, and policy impact. This transition is creating 
             a tremendous opportunity for Europe to demonstrate its ability to facilitate the implementation of sustainable, 
             resource-efficient, fair, and inclusive digital infrastructure. </p>
             <p>These first three years of the Horizon Europe 
             programme have seen several research and innovation initiatives start and gather momentum, while the originally 
             separated R&I communities around Cloud and IoT have increasingly come together in a new, seamless ecosystem: 
             EUCloudEdgeIoT provides coordination and support across this thriving landscape of projects and communities, 
             covering key horizontal enablers such as Open Source and Open Standards as well as verticalised stakeholder 
             engagement and market sectors. Further development is expected for 2024 and 2025, with new complementary 
             initiatives rolled out and updated vision and roadmap for Computing Continuum research.</p>
      
    - name: 'Award and Closing'
      presenter: <a href="speakers/#alexander_chatzigeorgiou">A. Chatzigeorgiou</a>,<br/> <a href="speakers/#david_jimenez">D. Jimenez</a>
      type: organization
      time: 16:25 - 16:45
       
    - name: 'Break'
      type: organization
      time: 16:45 - 17:25
      
    - name: 'Reception sponsored by Huawei'
      presenter: Bürgersaal Foyer
      type: eclipsecon
      time: 17:25 - 18:45
      abstract: |
             <p>Join the Eclipse community for the opening reception sponsored by Huawei.</p>
