---
title: TheiaCon 2023
seo_title: TheiaCon 2023
date: 2023-06-28T10:00:00-04:00
logo: '2023/theiacon/images/logos/theiacon-logo.svg'
headline: ''
custom_jumbotron: |
    <div class="theiacon-jumbotron-2023 padding-bottom-20">
        <div class="jumbotron-head">
            <div class="jumbotron-title-container col-md-14">
                <h1 class="jumbotron-title">TheiaCon 2023</h1>
                <p class="jumbotron-subtitle">Leading the Next Generation of Cloud IDE Development</p>
                <p class="jumbotron-details-text">VIRTUAL EVENT | November 15 - 16, 2023</p>
                <p>
                    <a class="btn btn-primary" href="https://forms.gle/c1WK9cS5NFUQymuU7" target="_blank">Propose a talk</a>
                    <a class="btn btn-primary" href="https://app.swapcard.com/event/theiacon-2023" target="_blank">Register now</a>
                </p>
            </div>
            <a class="col-xs-18 col-sm-12 col-md-10" href="https://outreach.eclipse.foundation/theia-ide-developer-tools">
                <div class="jumbotron-graphic-container gradient-backed-content-wrapper split-content">
                    <img class="gradient-backed-content" src="/2023/theiacon/images/theia-brief-tablet.webp" alt="Tablet showing Eclipse Theia project brief title page" />
                    <div>
                        <div class="bottom-aligned-text">
                            <p class="small">Learn more about Eclipse Theia</p>
                            <p class="padding-left-30">
                                <span class="brand-primary fw-700 uppercase">Project brief:</span><br />
                                Why Eclipse Theia is ideal to Build Modern IDEs and Developer Tools
                            </p>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
custom_jumbotron_class: container-fluid
header_wrapper_class: header-theiacon-event header-theiacon-event-2022
summary: 'TheiaCon is an annual virtual conference focused around the Eclipse Theia IDE ecosystem. It brings together a diverse group of Theia developers, adopters, and other contributors. The program will feature a mix of full-length talks, expert panel discussions and short lightning talks featuring project contributor insights, adopter stories, and work being done in the broader ecosystem.'
categories: []
keywords: ["TheiaCon", "TheiaCon 2023", "TheiaCon virtual conference", "virtual conference", "Theia developers conference", "Cloud DevTools Working Group"]
slug: ""
aliases: []
toc: false
draft: false
hide_page_title: true
hide_sidebar: true
container: container-fluid
layout: single
---
<!-- Registration -->

{{< grid/section-container id="registration" class="featured-section-row featured-section-row-lighter-bg">}}
{{< events/registration event="theiacon" year="2023" >}}
TheiaCon is an annual virtual conference focused around the Eclipse Theia IDE ecosystem. It brings together a diverse group of Theia developers, adopters, and other contributors. The program will feature a mix of full-length talks, expert panel discussions and short lightning talks featuring project contributor insights, adopter stories, and work being done in the broader ecosystem.
{{</ events/registration >}}
{{</ grid/section-container >}}

<!-- CFP -->
{{< grid/section-container id="cfp" class="featured-section-row featured-section-row-light-bg text-center" isMarkdown="false" >}}
<h2>Call for Presentations</h2>
<h4 class="margin-bottom-20">End Date: October 9, 2023</h4>

<a href="https://forms.gle/c1WK9cS5NFUQymuU7" class="btn btn-primary uppercase" target="_blank">Propose a talk</a>

{{</ grid/section-container >}}

<!-- Committee -->
{{< grid/section-container >}}
  {{< grid/div class="padding-top-40" id="committee" class="row padding-top-40" isMarkdown="false">}}
    {{< events/user_display event="theiacon" year="2023"  source="committee" imageRoot="/2023/theiacon/images/committee/" subpage="./program-committee">}}
    {{</ events/user_display >}}
  {{</ grid/div >}}
{{</ grid/section-container >}}

<!-- Agenda -->
{{< grid/section-container id="agenda" class="featured-section-row featured-section-row-lighter-bg text-center" isMarkdown="true" >}}

## Agenda

The agenda will be provided at a later date.
{{</ grid/section-container >}}

<!-- Sponsors -->
{{< theiacon/sponsored-section class="text-center padding-bottom-40" working_group="cloud-development-tools" display_sponsors="true" year="2023" event="theiacon" >}}
