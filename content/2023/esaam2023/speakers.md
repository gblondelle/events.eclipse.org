---
title: "eSAAM 2023 Speakers"
headline: ''
custom_jumbotron: |
    <h1 class="featured-jumbotron-title text-center">eSAAM 2023 Speakers</h1>
    <h2 class="featured-jumbotron-subtitle text-center">
        3rd Eclipse Security, AI, Architecture and Modelling Conference<br/>on Cloud to Edge Continuum
    </h2>
    <h3 class="featured-jumbotron-subtitle text-center" style="color: white;">October 17, 2023 | Ludwigsburg, Germany</h3>
custom_jumbotron_class: container-fluid
date: 2023-10-17T08:00:00-04:00
hide_page_title: true
hide_sidebar: true
header_wrapper_class: "header-esaam2023-event"
hide_breadcrumb: true
show_featured_footer: false
hide_call_for_action: true
main_menu: esaam2023  
container: "container-fluid esaam-2023-event"
summary: "eSAAM 2023 will bring together industry experts and researchers working on innovative software and systems solutions for the next generation of edge-cloud computing continuum, specifically  focusing on Security and Privacy, Artificial Intelligence and Machine Learning, Systems and Software Architecture, Modelling and related challenges. This event is co-located with EclipseCon 2023"
layout: single
keywords: ["eclipse", "UOM", "UPM", "conference", "research", "eSAAM", "SAAM", "Cloud", "Edge", "IoT", "Cloud-Edge-Continuum", "Edge-Cloud-Continuum", "Cloud Computing", "Security", "AI", "Architecture", "Modelling", "Modeling", "Registration"]
links: [ [href: "..", text: "Main Page"], [href: "https://eclipsecon.regfox.com/eclipsecon-2023", text: "Register Now"] ]
draft: false
---

{{< grid/section-container id="committee">}}
	{{< events/user_bios event="esaam2023" year="2023" source="speakers" imgRoot="/2023/esaam2023/images/speakers/" >}}
{{</ grid/section-container >}}