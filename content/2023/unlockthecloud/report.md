---
title: Cloud Market Research Study Presentation
seo_title: Cloud Market Research Study Presentation | Unlock the Cloud
hide_sidebar: true
layout: single
---

The Cloud service market is of primary importance, with a size of already nearly 190 B€ in the EU
in 2022 and a forecasted annual growth of 13% over the coming decade. It acts as a key enabler in
the majority of European industrial ecosystems.

Our report gives valuable insights into how Europe can addresses both legal and technical steps
needed to enable Cloud interoperability.

{{< grid/div class="margin-top-20 margin-bottom-20" isMarkdown="false" >}}
    <a class="btn btn-primary" href="../documents/unlock-the-cloud-interoperability-to-foster-the-eu-digital-market-report.pdf">DOWNLOAD THE REPORT</a>
{{</ grid/div >}}

The costs of cloud services have been rising rapidly in recent years, harming both the
competitiveness of European industry and the purchasing power of European end customers in the
context of inflation.

Despite the historical strengths of the EU in application software with leaders such as SAP,
Dassault Systèmes, and Siemens Software... **the EU ecosystem of Cloud Service Providers (CSP)
remains small and limited in its ability to grow**. EU players only own 16% market shares on the
EU SaaS market, and this share tends to decrease over the years.

This situation is largely due to the oligopolistic situation on the EU Cloud market with 90% market
shares being owned by three providers (AWS, Microsoft, and Google), inducing a distortion of
competition in three ways:

1. Consumers lock-in. Making it impossible for software vendors to replace their Cloud computing
   supplier with another one.
2. Software vendors lock-out. Making it impossible for Cloud Service Providers (CSP) to propose
   their services to SaaS developers already engaged with another CSP.
3. Unfair competition. Where a leading Cloud Service Provider (CSP) can use its position to hamper
   the quality of the offer of its competitors.

<!-- Image -->

{{< grid/div class="report-split-section gap-20 row margin-top-20" isMarkdown="false" >}}
    <div class="report-img-wrapper">
        <img class="report-img img img-responsive" src="../images/report.jpg" alt="" />
    </div>
    {{< grid/div isMarkdown="true" >}}

Such situation is a threat for the digital sovereignty of the EU. **The entire European economy is
facing a risk of dependence given the importance cloud-based software is having in segments of our
industry.**

However, this oligopolistic situation and its consequences could be solved through the building of
a legal and technical framework enabling full Cloud interoperability. The recent "Data Act"
proposal is the opportunity to build such a framework, it already contains regulatory obligations
related to Cloud interoperability and data portability, OSC would technical enable it.

**Open Services Cloud is an open-source framework that aims to simplify the inherent complexity of
the current Cloud offerings** by covering multiple aspects:

- **A semantic layer:** the framework comes with a cloud neutral semantic layer, the Open Services
  Cloud Configuration Language, OCL. It’s a descriptive language that allows, via a standardized code
  structure, to describe service and workload deployments in the Cloud.
- **An orchestration platform:** this component ensures consistency of the deployments across multiple
  Clouds. It enables users to deploy and manage applications on various Cloud platforms via a central
  console while providing in data management capabilities in parallel, removing Cloud lock-in.

Overall, the introduction of Open Services Cloud to the market would represent multiple advantages
that would benefit all parties involved:

1. **Integration costs optimization as** Cloud Service Providers and Software Editors face a challenge
   of cloud integration that will now be mitigated
2. **Market cost efficiency as** Open Services Cloud would incur synergies by reducing complexity to
   deploy in multiple Clouds, allowing all parties to simplify cross-Cloud workload deployment. This
   increased competitivity will lower overall costs as the lock-in effect would be reduced.
3. **Skills management as** in most countries, the market for Cloud subject matter experts is very
   tense, and software vendors can’t “templatize” their deployments due to the huge variety of Cloud
   platforms available.
4. **Cloud portability** would be ensured by providing a standardized way to deploy workloads in any
   Cloud. Open Services Cloud removes the barriers of redeploying workloads elsewhere, allowing for
   more mobility and drastically reducing the related frictions

    {{</ grid/div >}}
{{</ grid/div >}}

{{< grid/div class="row margin-top-40 margin-bottom-40 flex-center gap-20" isMarkdown="false" >}}
    <a class="btn btn-primary" href="../documents/unlock-the-cloud-interoperability-to-foster-the-eu-digital-market-report.pdf">DOWNLOAD THE REPORT</a>
    <a class="btn btn-primary" href="https://unlock-cloud.eventbrite.com/">JOIN OUR EVENT</a>
{{</ grid/div >}}

